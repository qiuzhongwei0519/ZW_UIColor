//
//  main.m
//  ZW_UIColor
//
//  Created by scottlove0519 on 07/17/2018.
//  Copyright (c) 2018 scottlove0519. All rights reserved.
//

@import UIKit;
#import "WWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WWAppDelegate class]));
    }
}
