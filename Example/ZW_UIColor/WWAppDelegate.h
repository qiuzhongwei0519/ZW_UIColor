//
//  WWAppDelegate.h
//  ZW_UIColor
//
//  Created by scottlove0519 on 07/17/2018.
//  Copyright (c) 2018 scottlove0519. All rights reserved.
//

@import UIKit;

@interface WWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
