#
# Be sure to run `pod lib lint ZW_UIColor.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name         = "ZW_UIColor"
s.version      = "0.0.6"
s.summary      = "ZW_UIColor"
s.description  = 'ZW_UIColor颜色分类'
s.homepage     = "https://gitee.com/qiuzhongwei0519/ZW_UIColor"
s.license      = "MIT"
s.author       = { "qiuzhongwei0519" => "512866393@qq.com" }
s.platform     = :ios
s.platform     = :ios, "5.0"
s.source       = {
:git => "https://gitee.com/qiuzhongwei0519/ZW_UIColor.git",
:tag => "#{s.version}"
}
s.source_files  = "ZW_UIColor/Classes/**/*.{h,m}"
s.frameworks = "UIKit", "Foundation"
end
